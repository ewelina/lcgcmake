#--- General parameters ----------------------------------------------------------------------------
set(Python_cmd ${Python_home}/bin/${PYTHON})
set(PySetupOptions --root=/ --prefix=<INSTALL_DIR>)

#---arrow---------------------------------------------------------------------------------------------
# Remember to rename the tarball to match url
LCGPackage_Add(
  arrow
  URL ${GenURL}/apache-arrow-${arrow_native_version}.tar.gz
  ENVIRONMENT      SNAPPY_HOME=${snappy_home} 
              FLATBUFFERS_HOME=${flatbuffers_home}
                RAPIDJSON_HOME=${rapidjson_home}
                   BROTLI_HOME=${brotli_home}
                   THRIFT_HOME=${thrift_home}
                   GLOG_HOME=${glog_home}
                   GLOG_ROOT=${glog_home}
                   Boost_ROOT=${Boost_home}
                   BOOST_ROOT=${Boost_home}
                   ARROW_JEMALLOC_URL=${GenURL}/jemalloc-${jemalloc_native_version}.tar.bz2
                   C_INCLUDE_PATH=${Boost_home}/include
                   CPLUS_INCLUDE_PATH=${Boost_home}/include
  CONFIGURE_COMMAND ${CMAKE_COMMAND} <SOURCE_DIR>/cpp 
                    -DCMAKE_BUILD_TYPE=Release
                    -DARROW_VERBOSE_THIRDPARTY_BUILD=ON
                    -DARROW_PYTHON=ON 
                    -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR> 
                    -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
                    -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
                    -DARROW_DEPENDENCY_SOURCE=SYSTEM
                    -DARROW_WITH_ZSTD=OFF
                    -DARROW_WITH_BROTLI=ON
                    -DARROW_USE_GLOG=OFF
                    -DARROW_WITH_LZ4=ON
                    -DARROW_WITH_SNAPPY=ON
                    -DSnappy_ROOT=${snappy_home}
                    -DGLOG_ROOT=${glog_home}
                    -DARROW_BUILD_TESTS=OFF 
                    -DARROW_BUILD_BENCHMARKS=OFF
                    -DARROW_PARQUET=ON
                    -DARROW_JEMALLOC=ON
                    -DARROW_USE_CCACHE=OFF
                    -DARROW_WITH_UTF8PROC=OFF
                    -DARROW_WITH_RE2=OFF
                    -DRAPIDJSON_INCLUDE_DIR=${rapidjson_home}/include # RapidJSONConfig.cmake defines only RAPIDJSON_INCLUDE_DIRS
                    ${Boost_extra_configuration} 
  BUILD_COMMAND ${MAKE} 
  INSTALL_COMMAND ${MAKE} install
  DEPENDS Boost Python numpy flatbuffers thrift brotli double_conversion glog gflags rapidjson lz4 snappy IF LCG_OS STREQUAL mac THEN autoconf ENDIF
)
#---Forward declarations----------------------------------------------------------------------------
#LCGPackage_set_home(numpy)

#---ROOT--------------------------------------------------------------------------------------------  
if(ROOT_native_version)
  string(REGEX MATCH "[0-9]+[.][0-9]+[.][0-9]+" ROOT_author_version "${ROOT_native_version}")
  if(ROOT_native_version MATCHES HEAD|master|fix)
    set(ROOT_version 99)
  else()
    string(REGEX REPLACE ".*([0-9]+)[.-]([0-9]+)[.-]([0-9]+).*" "\\1.\\2.\\3" ROOT_version ${ROOT_native_version})
  endif()
  set(ROOT_MINIMAL_CMAKE_VERSION 3.4.3)

  if(ROOT_version VERSION_LESS 6)
    # Special setup for NA61/SHINE ROOTv5 & 32bit. Because it should not be
    # mistakenly modified when usual ROOT version is considered, it is
    # delegated to a separate file.
    include(${CMAKE_CURRENT_LIST_DIR}/ROOT5.cmake)
  else()
  LCGPackage_Add(
      ROOT
      IF <VERSION> MATCHES "^t.*" THEN
        URL ${GenURL}/root-<VERSION>.zip
      ELSE
        URL http://root.cern.ch/download/root_v${ROOT_author_version}.source.tar.gz
      ENDIF
      IF CMAKE_VERSION VERSION_LESS ROOT_MINIMAL_CMAKE_VERSION THEN 
        CMAKE_COMMAND ${CMake_home}/bin/cmake
      ENDIF
      ENVIRONMENT MYSQL_HOME=${mysql_home}
      CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
                 -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
                 IF ROOT_version VERSION_LESS 6.22 THEN
                   -Dpython=ON
                 ELSE
                   -Dpyroot=ON
                 ENDIF
                 IF ROOT_version VERSION_LESS 6.22 AND Python_native_version VERSION_GREATER 3 THEN
                   -Dpython3=ON
                   -Dpython_version=3
                 ENDIF
                 IF ROOT_version VERSION_GREATER_EQUAL 6.22 THEN
                   -DPYTHON_EXECUTABLE=${Python_home}/bin/python
                 ENDIF
                 -Dbuiltin_lz4=ON
                 -Dbuiltin_pcre=ON
                 -Dbuiltin_xxhash=ON
                 -Dbuiltin_ftgl=ON
                 -Dbuiltin_afterimage=ON
                 -Dbuiltin_glew=ON
                 -Dbuiltin_unuran=ON
                 -Dbuiltin_zstd=ON
                 -Dcintex=ON
                 IF DEFINED cuda_native_version THEN
                   -Dcuda=ON
                   -Dtmva-gpu=ON
                   -DCMAKE_CUDA_STANDARD=${CMAKE_CUDA_STANDARD}
                 ENDIF
                 IF DEFINED Davix_native_version THEN
                   -Ddavix=ON
                 ELSE
                   -Ddavix=OFF
                 ENDIF
                 -Dexceptions=ON
                 -Dexplicitlink=ON
                 -Dfftw3=ON
                 -Dgdml=ON
                 -Dgsl_shared=ON
                 -Dhttp=ON
                 -Dgenvector=ON
                 IF ROOT_version VERSION_GREATER 6 AND NOT LCG_TARGET MATCHES mac AND NOT ${LCG_ARCH} STREQUAL i686 THEN
                   -Dvc=ON
                 ENDIF
                 -Dmathmore=ON
                 -Dminuit2=ON
                 IF DEFINED mysql_native_version THEN
                   -Dmysql=ON
                 ENDIF
                 -Dopengl=ON
                 -Dpgsql=OFF
                 -Dpythia6=OFF
                 -Dpythia8=OFF
                 -Dreflex=ON
                 -Dr=OFF
                 -Droofit=ON
                 -Dssl=ON
                 -Dunuran=ON
                 -Dfortran=ON
                 -Dxft=ON
                 -Dxml=ON
                 -Dxrootd=ON
                 -Dzlib=ON
                 -DCINTMAXSTRUCT=36000
                 -DCINTMAXTYPEDEF=36000
                 -DCINTLONGLINE=4096
                 IF ROOT_version VERSION_LESS 6.16 THEN
                    -Dgviz=ON
                    -Dqtgsi=ON
                    -Drfio=ON
                    -Dtable=ON
                    -Dkrb5=ON
                    -Dmemstat=ON
                    -Dldap=ON
                    -Dodbc=ON 
                 ELSE
                    -Dfail-on-missing=ON
                    -Dfitsio=ON
                    -Doracle=OFF
                    -Dgfal=OFF
                 ENDIF
                 IF ROOT_version VERSION_LESS 6.18 THEN
                   IF LCG_CPP14 THEN
                       -Dc++14=ON
                       -Dcxx14=ON
                   ENDIF
                   IF LCG_CPP17 THEN
                       -Dc++17=ON
                       -Dcxx17=ON
                   ENDIF
                 ELSE
                   -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
                 ENDIF
                 IF LCG_TARGET MATCHES x86_64-slc OR LCG_TARGET MATCHES x86_64-centos7 THEN
                   IF ROOT_version VERSION_LESS 6.16 THEN
                     -Dcastor=ON
                   ENDIF
                     -Ddcache=ON
                     -Dgfal=ON -DGFAL_DIR=${gfal_home}
                               -DSRM_IFCE_DIR=${srm_ifce_home}
                 ENDIF
                 IF DEFINED oracle_native_version AND (LCG_TARGET MATCHES slc OR LCG_TARGET MATCHES centos AND NOT LCG_TARGET MATCHES "aarch64") THEN
                     -Doracle=ON -DORACLE_HOME=${oracle_home}
                 ENDIF
                 IF ROOT_version VERSION_GREATER 6.22 THEN
                   IF DEFINED jsonmcpp_native_version THEN
                     -Dbuiltin_nlohmannjson=OFF
                   ELSE
                     -Dbuiltin_nlohmannjson=ON
                   ENDIF
                 ENDIF
      DEPENDS Python fftw GSL xrootd numpy tbb blas zlib libxml2 vdt xz gl2ps cfitsio
              IF DEFINED mysql_native_version THEN
                  mysql
              ENDIF
              IF DEFINED cuda_native_version THEN
                  cuda cudnn
              ENDIF
              IF DEFINED Davix_native_version THEN
                  Davix
              ENDIF
              IF ROOT_version VERSION_LESS 6.16 THEN
                  graphviz
              ENDIF
              IF LCG_TARGET MATCHES x86_64-slc OR LCG_TARGET MATCHES x86_64-centos7 THEN
                IF ROOT_version VERSION_LESS 6.16 THEN
                  CASTOR
                ENDIF 
                  dcap gfal srm_ifce
              ENDIF
              IF DEFINED oracle_native_version AND (LCG_TARGET MATCHES slc OR LCG_TARGET MATCHES centos AND NOT LCG_TARGET MATCHES "aarch64") THEN
                  oracle
              ENDIF
              IF ROOT_version VERSION_GREATER 6 AND NOT LCG_TARGET MATCHES mac AND NOT ${LCG_ARCH} STREQUAL i686 THEN
                  Vc
              ENDIF
              IF ROOT_version VERSION_GREATER 6.22 AND DEFINED jsonmcpp_native_version THEN
                  jsonmcpp
              ENDIF

  )
  endif()
  
  # Add the dependency to CMake but without changing the hash calculation
  if(CMAKE_VERSION VERSION_LESS ROOT_MINIMAL_CMAKE_VERSION)
    add_dependencies(ROOT CMake)
  endif()
  
  # Add extra ROOT external dependencies (tools, and optional components used to test ROOT)
  if(NOT APPLE)
    set(extradeps hadoop Qt)
  endif()
  foreach(dep postgresql openldap libaio gdb cfitsio pythia8 pythia6 R gridexternals
      pytools pango lcgenv libiodbc ninja CMake veccore vdt sas jupyter tbb valgrind ccache
      gcovr arrow libxml2 blas freetype GSL numpy pytest xrootd ${extradeps})
    if(DEFINED ${dep}_native_version OR TARGET ${dep})
      add_dependencies(ROOT-dependencies ${dep})
    endif()
  endforeach()
endif(ROOT_native_version)

#---RELAX---------------------------------------------------------------------------------------------
LCGPackage_Add(
  RELAX
  URL ${GenURL}/RELAX-${RELAX_native_version}.tar.gz
#  SVN_REPOSITORY http://svn.cern.ch/guest/relax/tags/${RELAX_native_version}/relax --quiet
  UPDATE_COMMAND <VOID>
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_MODULE_PATH=${CMAKETOOLS_MODULES}
             -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
             -DROOTSYS=${ROOT_home}
             IF DEFINED GCCXML_native_version THEN
               -DGCCXML=${GCCXML_home}/bin/gccxml
             ENDIF
             -DCLHEP_ROOT_DIR=${clhep_home}
             -DHEPMC_ROOT_DIR=${HepMC_home}
             -DGSL_ROOT_DIR=${GSL_home}
             -DHEPPDT_ROOT_DIR=${HepPDT_home}
  BUILD_COMMAND ${MAKE} ROOTSYS=${ROOT_home}
  DEPENDS cmaketools ROOT
          IF DEFINED GCCXML_native_version THEN
            GCCXML
          ENDIF
          clhep HepMC HepPDT GSL
)

#---GaudiDeps------------------------------------------------------------------------------------------
set(GAUDI-alldeps  AIDA Boost Python GSL ROOT clhep HepMC HepPDT RELAX
                   uuid tbb XercesC rangev3 cppgsl CppUnit QMtest nose gperftools six networkx ninja CMake xenv doxygen fmt jsonmcpp )

set(GAUDI-dependencies)
foreach(dep ${GAUDI-alldeps})
  if(DEFINED ${dep}_native_version)
    set(GAUDI-dependencies ${GAUDI-dependencies} ${dep})
  endif()
endforeach()

foreach(dep ${GAUDI-dependencies})
  if(TARGET install-${dep})
    list(APPEND GAUDI-installs install-${dep})
  endif()
endforeach()

add_custom_target(GAUDI-externals
                 COMMENT "Target to build all externals packages needed by Gaudi"
                 DEPENDS ${GAUDI-dependencies})
#---GeantVDeps-----------------------------------------------------------------------------------------
set(GeantV-alldeps ROOT VecGeom Python Boost fftw graphviz GSL mysql xrootd R numpy tbb blas pythia6
                   Geant4 XercesC clhep expat hepmc3 umesimd VecGeom Vc veccore vecmath benchmark ninja CMake lcgenv doxygen)
foreach(dep ${GeantV-alldeps})
  if(DEFINED ${dep}_native_version)
    set(GeantV-dependencies ${GeantV-dependencies} ${dep})
  endif()
endforeach()
add_custom_target(GeantV-externals
   COMMENT "Target to build all externals packages needed by GeantV"
   DEPENDS ${GeantV-dependencies} )

#---Geant4Deps-----------------------------------------------------------------------------------------
set(Geant4-alldeps ROOT VecGeom Python Qt5 tbb XercesC clhep expat ninja ccache CMake lcgenv hdf5 valgrind pybind11)
foreach(dep ${Geant4-alldeps})
  if(DEFINED ${dep}_native_version)
    set(Geant4-dependencies ${Geant4-dependencies} ${dep})
  endif()
endforeach()
add_custom_target(Geant4-externals
   COMMENT "Target to build all externals packages needed by Geant4"
   DEPENDS ${Geant4-dependencies} )

#---HSFDeps-----------------------------------------------------------------------------------------
 set(HSF-alldeps Boost ROOT GSL Python Qt5 tbb XercesC clhep Geant4)
 foreach(dep ${HSF-alldeps})
   if(DEFINED ${dep}_native_version)
     set(HSF-dependencies ${HSF-dependencies} ${dep})
   endif()
 endforeach()
 add_custom_target(HSF-testdrive
    COMMENT "Target to build all externals packages needed by HEP Test Stack"
    DEPENDS ${HSF-dependencies} )

#---SIO------------------------------------------------------------------------------------------------
LCGPackage_add(
  SIO
  URL ${GenURL}/SIO-${SIO_native_version}.tar.gz
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
             -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
             -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
             -DSIO_BUILTIN_ZLIB=OFF
  BUILD_COMMAND ${MAKE}
  INSTALL_COMMAND ${MAKE} install
      IF SIO_native_version VERSION_LESS 0.01_ THEN
          COMMAND ${EXEC} bash -c "mkdir -p <INSTALL_DIR>/lib/cmake/SIO"
          COMMAND ${EXEC} bash -c "cp -n -r <INSTALL_DIR>/*.cmake <INSTALL_DIR>/lib/cmake/SIO"
      ENDIF
  DEPENDS zlib
  REVISION 1
)

#---LCIO-----------------------------------------------------------------------------------------------
LCGPackage_add(
  LCIO
  URL ${GenURL}/LCIO-${LCIO_native_version}.tar.gz
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
             -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
             -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
             -DLCIO_GENERATE_HEADERS=off
             -DBUILD_ROOTDICT=ON
             -DROOT_DIR=${ROOT_home}
  BUILD_COMMAND ${MAKE} ROOTSYS=${ROOT_home}
  INSTALL_COMMAND ${MAKE} install
          COMMAND ${EXEC} bash -c "mkdir -p <INSTALL_DIR>/lib/cmake/LCIO"
          COMMAND ${EXEC} bash -c "cp -n -r <INSTALL_DIR>/*.cmake <INSTALL_DIR>/lib/cmake/LCIO"
  DEPENDS ROOT clhep
  # there is no explicit flag for SIO dependency in LCIO
  IF LCIO_native_version VERSION_GREATER_EQUAL 2.14 THEN
  DEPENDS_OPT SIO
  ENDIF
)

#---VecGeom------------------------------------------------------------------------------------------
if(LCG_ARCH MATCHES aarch64)
  set(_vecgeomvector empty)
  set(_vecgeomnbackend Scalar)
else()
  set(_vecgeomvector sse4.2)
  set(_vecgeomnbackend Vc)
endif()
LCGPackage_add(
  VecGeom
  URL ${GenURL}/VecGeom-${VecGeom_native_version}.tar.gz
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DGEANT4=OFF
             -DUSOLIDS=ON
             -DUSOLIDS_VECGEOM=ON
             -DROOT=OFF
             -DCTEST=OFF
             -DBUILTIN_VECCORE=OFF
              IF LCG_INSTRUCTIONSET THEN 
                -DVECGEOM_VECTOR=${LCG_INSTRUCTIONSET}
              ELSE
                -DVECGEOM_VECTOR=${_vecgeomvector}
              ENDIF
              IF DEFINED cuda_native_version THEN
                -DCUDA=ON
                -DCUDA_ARCH=75
                -DBACKEND=Scalar
                -DCMAKE_CUDA_STANDARD=${CMAKE_CUDA_STANDARD}
                -DUSE_NAVINDEX=ON
              ELSE
                -DBACKEND=${_vecgeomnbackend}
                -DBUILD_SHARED_LIBS=ON
              ENDIF
             -DGDML=ON
             -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
             -DCMAKE_CXX_COMPILER=${CMAKE_CXX_WRAPPER}
  BUILD_WITH_INSTRUCTION_SET 1
  DEPENDS Vc veccore XercesC
  DEPENDS_OPT cuda
  REVISION 3
)

#---Geant4---------------------------------------------------------------------------------------------
set(Geant4_datadir /cvmfs/geant4.cern.ch/share/data)
LCGPackage_add(
  Geant4
#  URL http://geant4.cern.ch/support/source/geant4.${Geant4_native_version}.tar.gz
  URL ${GenURL}/geant4.${Geant4_native_version}.tar.gz
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
             -DGEANT4_USE_GDML=ON
             -DXERCESC_ROOT_DIR=${XercesC_home}
             IF DEFINED clhep_native_version THEN
               -DGEANT4_USE_SYSTEM_CLHEP=ON
             ELSE
               -DGEANT4_USE_SYSTEM_CLHEP=OFF
             ENDIF
             -DGEANT4_USE_G3TOG4=ON
             IF DEFINED Qt5_native_version THEN
               -DGEANT4_USE_QT=ON
               -DGEANT4_USE_OPENGL_X11=ON
               -DGEANT4_USE_XM=ON
               -DGEANT4_USE_RAYTRACER_X11=ON
               -DGEANT4_BUILD_MULTITHREADED=ON
             ENDIF
             -DQT_QMAKE_EXECUTABLE=${Qt_home}/bin/qmake
             IF EXISTS ${Geant4_datadir} THEN
               -DGEANT4_INSTALL_DATADIR=${Geant4_datadir}
             ELSE
               -DGEANT4_INSTALL_DATA=ON
             ENDIF
             -DGEANT4_BUILD_TLS_MODEL=global-dynamic
             -DGEANT4_USE_SYSTEM_EXPAT=OFF
             -DGEANT4_INSTALL_PACKAGE_CACHE=OFF
             IF <VERSION> VERSION_LESS 10.04 THEN
               -DGEANT4_USE_USOLIDS=ON
               -DUSolids_DIR=${VecGeom_home}/lib/cmake/USolids/
             ELSE
               -DGEANT4_USE_USOLIDS=ALL
               -DVecGeom_DIR=${VecGeom_home}/lib/cmake/VecGeom
             ENDIF
             -DGEANT4_BUILD_CXXSTD=${CMAKE_CXX_STANDARD}
  BUILD_COMMAND ${MAKE}
  INSTALL_COMMAND make install
  DEPENDS     XercesC VecGeom expat motif
  DEPENDS_OPT Qt5 clhep
  REVISION 2
)

#---delphes--------------------------------------------------------------------
LCGPackage_add(
    delphes
    URL ${GenURL}/Delphes-${delphes_native_version}.tar.gz
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
               -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
               -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
    DEPENDS ROOT
)

#---hepmc3----------------------------------------------------------------------------
string(REPLACE "." "" Python_config_version_twodigit_nodot "${Python_config_version_twodigit}")
LCGPackage_Add(
    hepmc3
    URL ${GenURL}/HepMC3-${hepmc3_native_version}.tar.gz
    ENVIRONMENT ROOTSYS=${ROOT_home}
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DROOT_DIR=${ROOT_home}
               -DHEPMC3_INSTALL_INTERFACES=ON
               -DHEPMC3_Python_SITEARCH${Python_config_version_twodigit_nodot}=<INSTALL_DIR>/lib/python${Python_config_version_twodigit}/site-packages
               -DHEPMC3_PYTHON_VERSIONS=${Python_config_version_twodigit}
    BUILD_COMMAND ${MAKE} -j1
    DEPENDS ROOT
    REVISION 1
)

#---Garfield++-----------------------------------------------------------------
LCGPackage_Add(
    Garfield++
    URL ${GenURL}/garfieldpp-<VERSION>.tar.gz
    ENVIRONMENT ROOTSYS=${ROOT_home}
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
               -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
               -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
               -DWITH_EXAMPLES=OFF
    DEPENDS ROOT Geant4
)

#---devBE---------------------------------------------------------------------------
set(devBE-alldeps backports CMake cmaketools cx_oracle h5py jupyter_contrib_nbextensions
                  lapack lcgenv logilabcommon numexpr paramiko protobuf pylint pyqt5 
                  pyserial requests scikitlearn seaborn sortedcontainers sqlalchemy xrootd xz )
foreach(dep ${devBE-alldeps})
    if(DEFINED ${dep}_native_version)
        set(devBE-dependencies ${devBE-dependencies} ${dep})
    endif()
endforeach()

add_custom_target(devBE-externals
    COMMENT "Target to build all externals packages needed by devBE"
    DEPENDS ${devBE-dependencies}
)

#---unigen----------------------------------------------------------------------
LCGPackage_Add(
  unigen
  URL ${GenURL}/unigen-<NATIVE_VERSION>.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND ${MAKE}
  INSTALL_COMMAND ${MAKE} DESTDIR=<INSTALL_DIR> install
  BUILD_IN_SOURCE 1
  DEPENDS ROOT
  REVISION 1
)
