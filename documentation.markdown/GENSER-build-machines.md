GENSER has the following build machines for old platforms: 

1. genser-slc5-i686-4  for i686-slc5   - virtual - created by Ivan
2. ec-slc6-i686-spi-1  for i686-slc6 - SFT-SPI machine
3. genser-slc5-x8664     for x86-64-slc5  - virtual - created by Grigory
   
Login as sftnight user.
    
If the host name does not exist, you can do the following steps: 

a.  Go to https://openstack.cern.ch

b.  Launch instances for the necessary platform.

c.  `$ ssh instance_name`

d.  the following steps are necessary for lcgcmake running:

        $ sudo yum install glibc-devel
        $ sudo yum install subversion.i386
        $ sudo yum install git
        $ sudo yum install freetype-devel libpng-devel  bzip2-devel autoconf automake

e.  Add sftnight user:

        $ sudo /usr/sbin/addusercern sftnight

f.  Login as sftnight:
    
        $ ssh sftnight@instance_name
