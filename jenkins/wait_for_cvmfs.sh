#!/bin/bash -x
# Wait until today's nightly is available on stratum 1
# wait_for_cvmfs $VOLUME $REVISION

VOLUME=$1
REVISION=$2

for iterations in {1..12}
do
    if [ `uname -s` == Darwin ]; then
        current_rev=`xattr -p user.revision /cvmfs/$VOLUME`
    else
        current_rev=`attr -qg revision /cvmfs/$VOLUME/`
    fi
    if [ "${current_rev}" -ge "${REVISION}" ]
    then
        echo "[INFO] Current revision [${current_rev}] is greater or equal than the required [${REVISION}]."
        break
    else
        if  [[ "${iterations}" == "12" ]]
        then
            echo "[ERROR] Abort after one hour of waiting."
            exit 1
        else
            echo "[WARNING] Revision is not yet present on CVMFS stratum 1. Going to sleep for 5 minutes ..."
            sleep 300
        fi
    fi
done
exit 0
