#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT    v6-24-00-patches GIT=http://root.cern.ch/git/root.git)

#---Remove unneeded packages---------------------------------------
LCG_remove_package(Geant4)
LCG_remove_package(DD4hep)
LCG_remove_package(acts)
LCG_remove_package(Gaudi)
LCG_remove_package(Garfield++)

#---Overwrites and additional packages ----------------------------
LCG_external_package(Python        3.8.6                         ) # TensorRT does not provide binaries for Python 3.9 yet

LCG_external_package(cuda         11.2    full=11.2.1_460.32.03 )
LCG_external_package(cudnn        8.1.1.33                      )
LCG_external_package(TensorRT     7.2.3.4                       )
LCG_external_package(NsightSystems 2021.2 full=2021.2.1.58-642947b )

LCG_external_package(pycuda       2021.1                        )
LCG_external_package(appdirs      1.4.4                         )
LCG_external_package(py_tools     2021.2.7                      )
LCG_external_package(pybind11     2.6.2                         )
LCG_external_package(pyopencl     2021.2.2                      )
LCG_external_package(mako         1.1.4                         )
LCG_external_package(cupy         9.1.0                         )
LCG_external_package(fastrlock    0.6                           )

#----Overwites for Ubuntu20---------------------------------------
if( ${LCG_OS}${LCG_OSVERS} MATCHES ubuntu20 )
  LCG_remove_package(TensorRT)
endif()