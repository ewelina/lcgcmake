#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 2)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT    HEAD     GIT=http://root.cern.ch/git/root.git         )
LCG_external_package(hepmc3  HEAD     GIT=https://gitlab.cern.ch/hepmc/HepMC3.git  )
LCG_external_package(DD4hep  master   GIT=https://github.com/AIDASoft/DD4hep.git   )
LCG_remove_package(Gaudi)
LCG_external_package(pythia8           303            ${MCGENPATH}/pythia8 )
LCG_external_package(madgraph5amc      3.0.3.beta     ${MCGENPATH}/madgraph5amc author=3.0.3.beta)

#---Overwrites and not supported in Python 2-----------------------
include(heptools-python27)

#---Requires a fix in DD4hep----------------------------------------
LCG_remove_package(acts)
