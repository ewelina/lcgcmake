#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 2)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

# LCG_external_package(ROOT    v6-22-00-patches  GIT=http://root.cern.ch/git/root.git )
LCG_external_package(ROOT    v6-24-00-patches GIT=http://root.cern.ch/git/root.git)

LCG_external_package(madgraph5amc      2.8.1.atlas     ${MCGENPATH}/madgraph5amc author=2.8.1)

# Gaudi support python2 only until v34r0 and ROOT < v6.24.00 
LCG_remove_package(Gaudi)

#---Overwrites and not supported in Python 2-----------------------
include(heptools-python27)
