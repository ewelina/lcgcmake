#---List of externals
set(LCG_PYTHON_VERSION 3)
include(heptools-dev3)

# SWAN special packages
if( ${LCG_OS}${LCG_OSVERS} MATCHES centos7 AND ${LCG_COMP} MATCHES gcc )
  LCG_external_package(c_ares            1.17.1                                   )
  LCG_external_package(condor            8.9.11                                   )
  LCG_external_package(linux_pam         1.5.1                                    )
  LCG_external_package(myschedd          1.7-2                                    )
  LCG_external_package(ngbauth_submit    0.24-3                                   )
endif()

